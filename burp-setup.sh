#!/usr/bin/bash

SYSTEM_BURPSUITE_PATH="/usr/bin/burpsuite-pro"
# Get the current user's usrname
USRNAME=$(whoami)
BURPSUITE_DIRECTORY="/home/$USRNAME/.BurpSuite"

check_file_exists() {
    local file_path="$1"

    if [ -f "$file_path" ]; then
        echo -e "File $file_path exists. Proceeding...\n"
    elif [ -f "$BURPSUITE_DIRECTORY/$file_path" ]; then
        echo -e "File $file_path exists in $BURPSUITE_DIRECTORY. Proceeding...\n"
    else
        echo -e "File $file_path does not exist. Exiting...\n"
        exit 1
    fi
}

check_superuser() {
    if [ $(id -u) -eq 0 ]; then
        # User is root (superuser)
        echo "You are currently logged in as root."
        echo "Please proceed as a normal user. Exiting..."
        exit 1
    else
        # User is not root (normal user)
        echo -e "You are currently logged in as \033[0;32m$(whoami)\033[0m."
        echo -e "Proceeding with the script...\n\n\n"
        # Continue script execution here
    fi
}


print_banner() {
    # Define the banner text
    local BANNER_TEXT="BurpSuite Pro Setup"

    # Set the banner length
    local BANNER_LENGTH=${#BANNER_TEXT}

    local STAR_LINE="\033[0;33m*******************\033[0m"

    # Clear the screen before showing banner
    clear

    # Print a newline
    echo -e "\n"

    # Print the top border
    echo -e $STAR_LINE

    # Print the banner text in yellow color
    echo -e "\033[0;33m$BANNER_TEXT\033[0m"

    # Print the bottom border
    echo -e $STAR_LINE

    # Print newlines
    echo -e "\n\n"
}


create_burpsuite_desktop_file() {
    local local_desktop_file="burpsuite-pro.desktop"
    local desktop_file="/usr/share/applications/burpsuite-pro.desktop"
    local icon_path=$1  # Replace with the actual path to your SVG icon

    echo -e "Createing the desktop file with icon: $icon_path\n"

    # Create the desktop file content
    cat <<EOF > "$local_desktop_file"
[Desktop Entry]
Name=BurpSuite Pro
Encoding=UTF-8
Comment=Web Application Security Testing Platform
GenericName=Web Application Proxy
Exec=$SYSTEM_BURPSUITE_PATH
Icon=$icon_path
Type=Application
Terminal=false
StartupNotify=false
StartupWMClass=burpsuite-pro
Categories=WebAppAnalysis;WebApplicationProxies;
EOF

    echo "Local desktop file created: $local_desktop_file"

    # Ensure the local desktop file is executable
    chmod +x "$local_desktop_file"

    # Copy the local desktop file to /usr/share/applications if creation was successful
    if [ -f "$local_desktop_file" ]; then
        sudo cp "$local_desktop_file" "$desktop_file"
        echo "Desktop file copied to $desktop_file"
        rm "$local_desktop_file"  # Remove the local copy after copying to /usr/share/applications
    else
        echo "Error: Failed to create local desktop file."
        exit 1
    fi
}



download_burpsuite_pro_svg() {
    local url="https://portswigger.net/content/images/logos/burp-suite-professional.svg"
    local output=$1

    echo -e "Starting download from $url...\n"
    
    # Use wget to download the file
    wget -O "$output" "$url"
    
    # Check if wget succeeded
    if [ $? -eq 0 ]; then
        echo -e "Download successful. File saved as $output.\n"
    else
        echo -e "Error: Download failed. Please check the URL or your internet connection.\n"
        exit 1
    fi
}


download_burpsuite_pro_jar() {
    local url="https://portswigger-cdn.net/burp/releases/download?product=pro&type=Jar" # latest jar file download link
    local output=$1

    echo -e "Starting download from $url...\n"
    
    # Use wget to download the file
    wget -O "$output" "$url"
    
    # Check if wget succeeded
    if [ $? -eq 0 ]; then
        echo -e "Download successful. File saved as $output.\n"
    else
        echo -e "Error: Download failed. Please check the URL or your internet connection.\n"
        exit 1
    fi
}

create_burp_home_dir () {
    if [ -d "$BURPSUITE_DIRECTORY" ]; then
        echo -e "BurpSuite Directory Already Exists: $BURPSUITE_DIRECTORY"
    else 
        # creating `/home/$USRNAME/.BurpSuite` directory
        echo -e "Creating $BURPSUITE_DIRECTORY directory...\n"
        mkdir -p $BURPSUITE_DIRECTORY
    fi
}

check_and_move_the_file () {
    local THE_FILE="$1"

    check_file_exists $THE_FILE
    if [ -f "$BURPSUITE_DIRECTORY/$THE_FILE" ]; then
        echo -e "File $THE_FILE exists in $BURPSUITE_DIRECTORY. Proceeding...\n"
    else
        echo -e "Moving the $THE_FILE into the $BURPSUITE_DIRECTORY directory...\n"
        mv $THE_FILE $BURPSUITE_DIRECTORY
    fi
}

main(){
    print_banner

    check_superuser

    local BURPSUITE_ICON_PATH="burpsuite-pro.svg"
    local BURPSUITE_FILE_NAME="burpsuite_pro.jar"

    local BURPSUITE_KEY_FILE="BurpLoaderKeygen117.jar"
    local BURPSUITE_JAR_FILE=$BURPSUITE_FILE_NAME
    
    local BURPSUITE_ICON_IN_HOME="$BURPSUITE_DIRECTORY/$BURPSUITE_ICON_PATH"

    create_burp_home_dir

    check_and_move_the_file $BURPSUITE_KEY_FILE
    
    check_and_move_the_file $BURPSUITE_ICON_PATH

    if [ -f "$BURPSUITE_FILE_NAME" ]; then
        # Moving the burpsuite.jar into the directory
        echo -e "File $BURPSUITE_FILE_NAME exists. Proceeding...\n"
        echo -e "Moving the $BURPSUITE_FILE_NAME into the $BURPSUITE_DIRECTORY directory...\n"
        mv $BURPSUITE_FILE_NAME $BURPSUITE_DIRECTORY
    elif [ -f "$BURPSUITE_FILE_NAME" ]; then
        echo -e "Already File $BURPSUITE_FILE_NAME exists in $BURPSUITE_DIRECTORY. Proceeding...\n"
    else
        echo -e "File $BURPSUITE_FILE_NAME does not exist.\n"
        download_burpsuite_pro_jar $BURPSUITE_FILE_NAME && check_and_move_the_file $BURPSUITE_FILE_NAME
    fi

    # Entering into `/home/$USRNAME/.BurpSuite` directory
    echo -e "Entering into $BURPSUITE_DIRECTORY directory...\n"
    cd $BURPSUITE_DIRECTORY

    if [ -f "$BURPSUITE_ICON_IN_HOME" ]; then
        echo -e "File $BURPSUITE_ICON_IN_HOME exists. Proceeding...\n"
    else
        echo -e "File $BURPSUITE_ICON_IN_HOME does not exist. Downloading...\n"
        download_burpsuite_pro_svg $BURPSUITE_ICON_PATH
    fi

    if [ -f "$BURPSUITE_FILE_NAME" ]; then
        echo -e "File $BURPSUITE_FILE_NAME exists. Proceeding...\n"
    else
        echo -e "File $BURPSUITE_FILE_NAME does not exist. Downloading...\n"
    fi


    # execute Keygenerator
    echo -e 'Starting Keygenerator\n'
    (java -jar $BURPSUITE_DIRECTORY/$BURPSUITE_KEY_FILE) &
    sleep 3s

    # Execute Burp Suite Professional with Keyloader
    echo 'Executing Burp Suite Professional with Keyloader'

    local JAVA_SWING="--add-opens=java.desktop/javax.swing=ALL-UNNAMED"
    local JAVA_BASE="--add-opens=java.base/java.lang=ALL-UNNAMED"
    local JAVA_INTERNAL_ORG="--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED"
    local JAVA_INTERNAL_ORG_TREE="--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED"
    local BURPSUITE_KEY_LOCATION="-javaagent:$BURPSUITE_DIRECTORY/$BURPSUITE_KEY_FILE"
    local BURPSUITE_FILE_LOCATION="$BURPSUITE_DIRECTORY/$BURPSUITE_FILE_NAME"

    local NEW_BURPSUITE_PRO_FILE_CMD="java \"$JAVA_SWING\" \"$JAVA_BASE\" \"$JAVA_INTERNAL_ORG\" \"$JAVA_INTERNAL_ORG_TREE\" \"$BURPSUITE_KEY_LOCATION\" -jar \"$BURPSUITE_FILE_LOCATION\""
    local NEW_BURPSUITE_PRO_FILE="$NEW_BURPSUITE_PRO_FILE_CMD &"
    echo $NEW_BURPSUITE_PRO_FILE > burp

    # 'java "--add-opens=java.desktop/javax.swing=ALL-UNNAMED" "--add-opens=java.base/java.lang=ALL-UNNAMED" "--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED" "--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED" "-javaagent:/home/$USRNAME/.BurpSuite/BurpLoaderKeygen117.jar" -jar "/home/$USRNAME/.BurpSuite/burpsuite_pro" &' > burp

    chmod +x burp
    sudo cp burp $SYSTEM_BURPSUITE_PATH

    create_burpsuite_desktop_file "$BURPSUITE_ICON_IN_HOME"

    ($SYSTEM_BURPSUITE_PATH)
}

main
