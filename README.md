# Burp Suite Professional Installation steps for Linux

Burp Suite Professional is the world's most popular tool for web security testing. 


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Also Latest File will be automatically downloaded. Else

[File Link](https://edge11.110.ir.cdn.ir/Files/Software/burpsuite_profissional_v2023.10.1.2_Downloadly.ir.rar)

## Installation

```bash
git clone https://gitlab.com/ciqe/burpsuite-pro-any.git
```

```bash
cd burpsuite-pro-any
```

```bash
chmod +x burp-setup.sh
```

```bash
./burp-setup.sh
```
### One Liner

```bash
git clone https://gitlab.com/ciqe/burpsuite-pro-any.git && cd burpsuite-pro-any && chmod +x burp-setup.sh && ./burp-setup.sh
```

## After Above

1. Modify License String like "license to cybercommunity03".
2. Copy License key from keygen.jar and paste in Burp Suite Pro and click Next.
3. Select Manual Activation Option on your bottom Right in Burp Suite Pro.
4. Copy License Request from BurpSuite_Pro and paste in Keygenerator.
5. Copy license response from Keygenerator and paste in Burp Suite Pro, then next and Done.

## Command to Put in Desktop Icon

Demo,

> The Desktop Icon File will be automatically created by running the script. Else:

```bash
java "--add-opens=java.desktop/javax.swing=ALL-UNNAMED" "--add-opens=java.base/java.lang=ALL-UNNAMED" "--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED" "--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED" "-javaagent:/home/kali/.BurpSuite/BurpLoaderKeygen117.jar" -jar "/home/kali/.BurpSuite/burpsuite_pro.jar"
```

## Add your files

Push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ciqe/burpsuite-pro-any.git
git branch -M main
git push -uf origin main
```

